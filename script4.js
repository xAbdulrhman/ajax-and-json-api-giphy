let btn = document.querySelector('header button');
let searchText = document.querySelector('header input[type="text"]');
let searchResult = document.getElementById("searchResults");

btn.addEventListener("click", function() {
    searchResult.innerHTML = ""
    fetchy(searchText.value)
})

function fetchy(keyword) {
    if (!keyword) {
        return;
    }


    var url = "https://api.giphy.com/v1/gifs/search"
    var params = "api_key=eC4FlFP64xuoDe4INbWwgWPtxNeNSX1Q&limit=5&q=" + encodeURIComponent(keyword);

    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === 4 && this.status === 200) {
            processResponse(this.responseText);
        }
    });

    xhr.open("GET", url + "?" + params);

    xhr.send();
}

function processResponse(responseText) {
    var resp = JSON.parse(responseText);

    for (item of resp.data) {
        let imgElement = document.createElement("img");
        imgElement.src = item.images.downsized_medium.url;
        imgElement.alt = item.title;
        searchResult.appendChild(imgElement);
    }
}